# SCL CSC Scripts

Script(s) used for maintaining the CSC stat leaderboards on the SCL spreadsheet. 

Spreadsheet:
https://docs.google.com/spreadsheets/d/1eZ95Mqso0EyXXrLxVBLA0FQJL80pEZ3cAuFXFpZ40lw/edit#gid=0

CHSC Discord: 
https://discord.gg/NZKQ4Ax

## What it does

This script generates both all-time + per-pack statistics, and then dumps them into a designated range on the spreadsheet.
The monthly statistics are built from the corresponding month's sheet, and the all-time statistics are built from the resulting monthly statistics tables.
It's done in two steps, as doing it all at once will eventually cause the script to time out after enough months have been added -- being able to update each month's table individually keeps times low.

The script only calculates the following stats: cumulative score, number of score submissions, number of FCs, and number of wins (podium places - 1st, 2nd, or 3rd.)

This script does not do any text or cell formatting, data validation, or sanity checking. Typing 'balls' where a score is supposed to go will break it; it's not smart and it's not supposed to be.

Eventually its functionality may be extended e.g. to make inserting new scores easier, to automatically set up new sheets, or to provide additional stats. 

## How to use it

On the 5 Fret spreadsheet, there is an extra menu item called "CSC Leaderboards". Click this, then click "Build all LBs".

This will build both the cumulative all-time CSC leaderboard, and each pack's cumulative leaderboards.

For the first time, you'll have to grant the script permissions to access the spreadsheet from your account. This is just a consequence of how Google handles and trusts custom scripts.

## How to set up new months

The script relies heavily on the use of named ranges, which are just sections of the spreadsheet that get a named reference to use.
You can see the list by clicking 'Data', then 'Named ranges'.

There's a named range for every CSC song leaderboard, one named range for each monthly leaderboard, and one for the all-time leaderboard.
Named ranges for songs cover from the Rank 1 cell, down to the %age cell for the last rank (e.g. rank 25). They're named 'CSC\_MMMXX\_YY' where MMM is the first 3 letters of the month, XX is the year, and YY is an index starting from 01.
e.g. a valid range name is 'CSC\_Nov19\_21', or 'CSC\_May19\_05'.

Named ranges for monthly leaderboards cover from the first name (first cell below the name header) to the first cell below the # of FCs header. They're named 'CSC\_MMMXX\_LB'.

When a new pack gets released, a spreadsheet maintainer will have to do the following:

- Copy an existing CSC spreadsheet into a new sheet - right click the sheet name, and click 'Duplicate'.
- Rename the sheet accordingly (e.g. Copy of CSC November -> CSC December).
- Clear the existing scores, song names, month, and replace them with updated information. 
- Click 'Data', then 'Named ranges'.
- Scroll through the named ranges until you see the ones named "'[New Sheet Name]'!CSC\_MMMXX\_YY".
- Rename these named ranges to match the standards laid out above.
- Create or remove any named ranges as necessary; there should be as many named ranges as there are songs.
- Go to the 'CSC Stat Leaderboard' sheet, and copy + paste an existing leaderboard to the right of the old one.
- [Protip: after copy + pasting, copy the top row of cells from another leaderboard, then click the leftmost cell of the new leaderboard, and right-click -> Paste Special -> Paste column widths only, to get the leaderboard columns sized properly]
- Rename the new leaderboard (e.g. November 2019 -> December 2019)
- Draw a 5 by 1 cell from the first cell under the Name column, to the first cell under the '# of FCs' column.
- Right-click, and click 'Define the named range'. Name it according to the standards laid out above (e.g. CSC\_Dec19\_LB).
- Remove the old names + stats, but do not clear the Rank column.

The script maintainer will have to do the following:
- Add a new entry for the new pack in the packs array in the fullRebuild() function

For any questions, concerns, or when anything catches fire, contact LightlessWalk#1729 on Discord. Can't guarantee that they'll be of much help, though (^:
