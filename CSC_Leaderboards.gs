// Version: 0.3
// Author: LightlessWalk#1729
//
// This script provides various functions to build cumulative stat leaderboards for the CSC pack scores that the CHSC Discord server tracks.
// It depends heavily on existing spreadsheet maintenance standards, and formatting -- this script provides basically no input validation, formatting, or sanity checks.
// It's also written by someone with next to no proper programming experience, and even less JS experience, so abandon all hope ye who enter.
//
// See README.md for further information on how this script is used, and how the spreadsheet needs to be prepared. 
// Repo: https://gitlab.com/Vola/scl-csc-scripts

// Adds the CSC scripts menu item to the spreadsheet toolbar.

function onOpen() {
  let spreadsheet = SpreadsheetApp.getActive();
  let menuItems = [ {name: 'Build all LBs', functionName: 'fullRebuild'} ];
  
  spreadsheet.addMenu('CSC Leaderboards', menuItems);
}

// For testing purposes only.
function clearLBs() {
  
  let ss = SpreadsheetApp.getActiveSpreadsheet(),
      cscLBSheet = ss.getSheetByName('CSC Stat Leaderboard'),
      ranges = cscLBSheet.getNamedRanges();

  for (let lb of ranges) {
    lb.getRange().clearContent();
  }
}
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Leaderboard building functions
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

// Builds all the monthly pack leaderboards and the all time LB.
// A full build needs to be done every time scores are added to the spreadsheet, as there is no robust or reasonable way to do it as an incremental update
// (at least, none that I can think of)
// Be wary of script execution times. This is unsustainable and eventually this function will take too long to complete.
function fullRebuild() {
  
    // Add new packs here as they're released.
    const packs = ['CSC_May19', 'CSC_Jun19', 'CSC_Jul19', 'CSC_Aug19', 'CSC_Sep19', 'CSC_Oct19', 'CSC_Nov19', 'CSC_Dec19',
                   'CSC_Jan20', 'CSC_Feb20', 'CSC_Mar20', 'CSC_Apr20', 'CSC_May20', 'CSC_Jun20', 'CSC_Jul20'];
  
    for (const pack of packs) {
      buildLB(pack, `${pack}_LB`);
    }

    buildAllTimeLB();
}

// Combines all the monthly leaderboards together into one big all-time leaderboard.
// There's basically two steps to making the all-time leaderboard - build the monthly leaderboards out of each sheet, then build the all-time leaderboard out of the monthly ones.
// This is done to keep script execution times down. Scripts can run for about a minute before they time out.
function buildAllTimeLB() {
  
  let ss = SpreadsheetApp.getActiveSpreadsheet(),
      cscLBSheet = ss.getSheetByName('CSC Stat Leaderboard'),
      ranges = cscLBSheet.getNamedRanges();
      
  let lastUpdatedTime = cscLBSheet.getRange("B5"),
      allTimeResults = [];
  
  for (const lb of ranges) {
    
    // Ignore the CSC_AllTime_LB named range in the ranges array -- that way we can just add the rest
    if (lb.getName() !== 'CSC_AllTime_LB') {
      
      let temp_arr = lb.getRange().getValues();
      temp_arr.sort(arraySortByFirst);
      
      allTimeResults = addArrays(allTimeResults, temp_arr, 0);
      
    }
  }
  
  // Sort by score
  allTimeResults.sort(arraySort);
  
  // Finally, write it into the spreadsheet.
  writeLB(ss, allTimeResults, "CSC_AllTime_LB");
  
  // Write in the last time the script was ran
  let d = new Date();
  let timestamp = "Last updated: " + d.toUTCString();
  
  lastUpdatedTime.setValue(timestamp);
  lastUpdatedTime.setFontColor("white");
  
  
}

// packPrefix - CSC_ + 3 letter month + 2 digit year code corresponding to the pack to build / named ranges to filter for (e.g. 'CSC_May19' for CSC_May19_xx ranges)
// monthRange - name of corresponding named range for where the generated leaderboard gets placed
// 
// Very similar to the buildAllTimeLB() function, should combine them.
function buildLB(packPrefix, monthRange) {
  
  let ss = SpreadsheetApp.getActiveSpreadsheet(),
      ranges = ss.getNamedRanges(),
      statsArray = [];
  
  // Retrieve all ranges belonging to a particular pack denoted by the prefix, except for its all time LB range
  let cscRanges = ranges.filter(
    function (r) { 
      return r.getName().indexOf(packPrefix) !== -1 && r.getName().indexOf("LB") == -1
    }
  );
  
  let i, j;
  
  for (i = 0; i < cscRanges.length; ++i) {
    
    // 4 cols, x rows: cols are Rank, Name, Score, Accuracy
    let songData = cscRanges[i].getRange().getValues();

    // A series of manipulations to array contents.
    for (j = 0; j < songData.length; ++j) {
      
      // Eliminate all empty entries + reduce the size of the array to match
      let name = songData[j][1];
      
      if (name == '') {
       
        songData.splice(j, (songData.length - 1));
        break;
        
      }
      
      // Clean the names e.g. 'Badskillz (Hard)' to 'Badskillz'
      songData[j][1] = cleanNames(songData[j][1]);
      
      let rank = songData[j][0],
          fc = songData[j][3];
      
      // Indicate if player podium placed
      if (rank == 1.0 || rank == 2.0 || rank == 3.0) {
        songData[j][0] = 1;
      } else { 
        songData[j][0] = 0; 
      }
      
      // Indicate if player FC'd
      if (fc == 'FC') {
        songData[j][3] = 1;
      } else {
        songData[j][3] = 0;
      }
          
      // Force another column for counting number of scores submitted.
      songData[j].push(1);
      
    }
    
    // Sort array by name
    songData = songData.sort(arraySort);

    // Merge everything together
    // Technically old arrays aren't getting deleted, but they are getting shrunk to zero elements -- good enough!!!
    statsArray = addArrays(statsArray, songData, 1);        
    
  }
  
  // Format statsArray to match how it's presented on the spreadsheet.
  // Old:  # Win | Names | Score | # FCs | # Sub
  // New:  Names | Score | # Sub | # Win | # FCs

  for (let entry of statsArray) {
   
    // Is there not a better way to do this in GS?
    const wins  = entry[0], 
          names = entry[1], 
          score = entry[2],
          fcs   = entry[3],
          subs  = entry[4];
    
    entry[0] = names;
    entry[1] = score;
    entry[2] = subs;
    entry[3] = wins;
    entry[4] = fcs;
    
  }

  // Sort by score
  statsArray.sort(arraySort);

  // When songs w/ no score submissions are added in a 'blank' player gets created -- check + remove that here
  if (statsArray[statsArray.length - 1][0] === '') {
      statsArray.pop();
  }
  
  // Finally, write it into the spreadsheet.
  writeLB(ss, statsArray, monthRange);
  
}

// Writes a leaderboard array into the spreadsheet at its corresponding named range.
// spreadsheet = active spreadsheet object
// statsArray = leaderboard / stats array to write
// statsRange = named range to write it to, as string
function writeLB(spreadsheet, statsArray, statsRange) {
  
  // If there's nothing to write, don't bother
  if (statsArray.length == 0) { return; }
  
  // Set cell ranges for stats array to be written
  let rows = statsArray.length,
      lbRange = spreadsheet.getRange(statsRange),                
      updatedRange = lbRange.offset(0, 0, rows),            // To reset statsRange to fit the new amount of data
      bufferRange = lbRange.offset(rows, 0, 100);           // Wipe out old rankings, if players were removed / renamed
  
  // Update the named LB range to match size of player data
  spreadsheet.setNamedRange(statsRange, updatedRange);
  
  // The money shot
  updatedRange.setValues(statsArray);
  bufferRange.clearContent();
  
}


// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// Extra data manipulation functions
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=


// (Tries to) remove only percentages and difficulty names in parentheses, at the end of a player's name -- including extra spaces
// In case someone really wants to use a username w/ a bracketed section.
function cleanNames(username) {
  
  const regex1 = /(\(((Easy)|(Medium)|(Hard))\))/;    // Remove difficulty names
  const regex2 = /(\((\d*\%)\)($|\s*$))/;             // Remove speed indicators (shouldn't actually matter for CSC, but it's here just in case)
  const regex3 = /(\((6-[fF]ret)\))/;                 // Remove 6-fret indicator (just for Badskillz!!)
  
  username = username.replace(regex1, '').trim();
  username = username.replace(regex2, '').trim();
  username = username.replace(regex3, '').trim();
  
  return username; 
  
}


// Just in case named ranges get used elsewhere in each sheet
function getCSCRanges(namedRange) {
  if (namedRange.getName().indexOf('CSC_') !== -1) {
    return namedRange;
  }
}


// Thank you SO
// Sorts by the second column from highest to lowest, used in this script for both sorting names and scores
function arraySort(a, b) {
  
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] > b[1]) ? -1 : 1;      
    }
  
}

// Same as above, but for the first column i.e. when building the all-time leaderboard
// Wouldn't it be nice to specify which col. / index to sort by as well when using this sort function? Can JS do that? 
function arraySortByFirst(a, b) {
  
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] > b[0]) ? -1 : 1;      
    }
  
}


// Merges two song score data arrays -- 2D arrays, x cols by y rows -- together. Requries inputs are alphabetically sorted by username.
// How it works: take 2 2D arrays as input, and an empty output array. Compare the elements given by 'index' in the 2 input arrays. Move whatever's higher to the output array. If they're the same, move + add both. 
// Kind of like a zipper, or two lanes of traffic merging.
// This function is always used in reference to a list sorted + compared by player name. When building monthly LBs this refers to index 1 (rank, _name_, ...), for the all-time LB, this is index 0 (_name_, score, ...).
function addArrays(arr1, arr2, index) {

  	let result = [];
    
    while ((arr1.length > 0) && (arr2.length > 0)) {
    
   		if (arr1[0][index] > arr2[0][index]) {
        
        	result.push(arr1.shift());
            
        } else if (arr1[0][index] < arr2[0][index]) {
        
        	result.push(arr2.shift());
            
        } else {
        
        	let res1 = arr1.shift(),
                res2 = arr2.shift(),
                finalRes = [];
          
            for (var i = 0; i < res1.length; ++i) {
              
              if (i == index) {
                finalRes[i] = res1[i];
              } else {
                finalRes[i] = +res1[i] + res2[i];
              }
              
            }

            result.push(finalRes);

        	}

        }


    // Upper while loop terminates after one array shrinks to zero, so we move the contents from the remaining array to the result one.
    // We don't know which array will be smaller, so we do both -- could easily check that at the top of the function, but /shrug  
    while (arr1.length > 0) {
    	result.push(arr1.shift());
    }
    
    while (arr2.length > 0) { 
    	result.push(arr2.shift());
    }
    
    return result;
        
}
